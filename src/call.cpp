#include "http.hpp"

#include<thread>

using namespace HTTP;

string trim(string str)
{
	while(true) {
		if(str.empty()) return str;
		if(str.front() != ' ' && str.front() != '\t' && str.front() != '\r') break;

		str.erase(0, 1);
	}

	while(true) {
		if(str.empty()) return str;
		if(str.back() != ' ' && str.back() != '\t' && str.back() != '\r') break;

		str.pop_back();
	}

	return str;
}

bool Call::isReady()
{
	return socket.isReady();
}

bool Call::hasTimedOut()
{
	if(isReady()) return false;
	return (getTimestamp() > timestamp + TIMEOUT);
}

void Call::handle()
{
	readRequest();

	try {
		parseHeader();

		if(path.find('?') != string::npos) parsePath();

		parseBody();
		parseCookies();

		const auto& vhost = server->getVirtualHost(*this);

		server->log(socket.getAddress() + " called " + path);

		const auto handler = vhost.getHandlerForPath(path);

		if(handler) {
			operator<<(handler->handle(*this, vhost));
		} else {
			operator<<(vhost.getErrorHandler(NOT_FOUND)->handle(*this, vhost));
		}
	} catch(const bad_request_exception& e) {
		const auto& vhost = server->getDefaultVirtualHost();
		operator<<(vhost.getErrorHandler(BAD_REQUEST)->handle(*this, vhost));
	}
}

void Call::readRequest()
{
	while(socket.isReady()) buffer += socket.read();
}

bool Call::buffer_get(char& c)
{
	++i;

	if(i >= buffer.size()) return false;

	c = buffer[i];

	return true;
}

bool Call::buffer_nextLine(string& s)
{
	const auto search = buffer.find('\n', i + 1);

	if(search == string::npos) return false;

	s = string(buffer.begin() + i, buffer.begin() + search);
	i = search + 1;

	return true;
}

void Call::parseHeader()
{
	string line;

	bool first = true;

	while(buffer_nextLine(line)) {
		if(!line.empty()) {
			if(line.front() == '\r') break;
		}

		if(first) {
			const auto head = str_split(line, ' ');

			if(head.size() != 3) {
				throw bad_request_exception();
			}

			method = head[0];
			path = head[1];
			httpVersion = head[2];

			first = false;
			continue;
		}

		string name, value;
		bool n = true;

		for(const auto& c : line) {
			if(!n) {
				value += c;
				continue;
			}

			if(c == ':') {
				n = false;
			} else {
				name += c;
			}
		}

		name = trim(name);
		value = trim(value);

		httpParameters.emplace(name, value);
	}
}

void Call::parsePath()
{
	// TODO Handle percent-encoding

	auto split = str_split(path, '?');

	path = split[0];
	const auto args = (split.size() == 2 ? split[1] : "");

	split = str_split(args, '&');

	for(const auto& a : split) {
		const auto s = str_split(a, '=');

		if(s.size() > 2) throw bad_request_exception();

		getArgs.emplace(s[0], (s.size() == 2 ? s[1] : ""));
	}
}

void Call::parseBody()
{
	char c;

	while(buffer_get(c)) body += c;

	const auto split = str_split(body, '&');

	for(const auto& a : split) {
		const auto s = a.find('=');

		string name, value;

		if(s != string::npos) {
			name = string(a.begin(), a.begin() + s);
			value = string(a.begin() + s + 1, a.end());
		} else {
			name = a;
		}

		postArgs.emplace(name, value);
	}
}

void Call::parseCookies()
{
	const auto search = httpParameters.find("Cookie");
	if(search == httpParameters.end()) return;

	const auto split = str_split(search->second, ';');

	for(const auto& c : split) {
		const auto s = str_split(c, '=');

		auto name = s[0],
			 value = s[1];

		name = trim(name);
		value = trim(value);

		cookies.emplace(name, value);
	}
}

const string* Call::getHTTPParameter(const string&& name) const
{
	const auto search = httpParameters.find(name);
	return (search != httpParameters.end() ? &search->second : nullptr);
}

void Call::operator<<(Response& response)
{
	socket.writeLine(response.toString((method != "HEAD")));
	server->log(to_string(response.getResponseCode()) + " responded to "
		+ socket.getAddress() + " for path " + path);
}
