#include "http.hpp"
#include "handler/handler.hpp"

using namespace HTTP;

VirtualHost::VirtualHost(const string& host)
	: host{host}
{
	registerHandler<AssetsHandler>();

	registerErrorHandler<BadRequestHandler>(BAD_REQUEST);
	registerErrorHandler<NotFoundHandler>(NOT_FOUND);
	registerErrorHandler<RequestTimeoutHandler>(REQUEST_TIMEOUT);
}

void VirtualHost::registerStaticHandlers(const string& file)
{
	const auto content = readFile(file);
	if(content.empty()) return;

	json j;

	try {
		j = json::parse(content);
	} catch(const exception& e) {
		return;
	}

	if(j.count("handlers") != 1) return;
	if(!j["handlers"].is_array()) return;

	for(const auto& h : j["handlers"]) {
		registerHandler(h);
	}
}

void VirtualHost::registerHandler(const json& j)
{
	if(j.count("pages") != 1) return;
	if(!j["pages"].is_array()) return;

	if(j.count("path") == 1) {
		if(!j["path"].is_string()) return;

		const string path = j["path"];
		vector<string> pages;

		for(const auto& p : j["pages"]) {
			if(!p.is_string()) return;
			pages.push_back(p);
		}

		handlers.emplace_back(new StaticHandler(path, pages));
	} else if(j.count("error") == 1) {
		if(!j["error"].is_number()) return;

		const ResponseCode error = j["error"];
		vector<string> pages;

		for(const auto& p : j["pages"]) {
			if(!p.is_string()) return;
			pages.push_back(p);
		}

		errorHandlers.erase(error);
		errorHandlers.emplace(error,
			unique_ptr<Handler>(new StaticErrorHandler(error, pages)));
	}
}

const Handler* VirtualHost::getHandlerForPath(const string& path) const
{
	const auto p = find_if(handlers.cbegin(), handlers.cend(),
		[&path](const shared_ptr<Handler>& h)
		{
			if(h->isAbsolute()) {
				return (path == h->getPath());
			} else {
				return str_startsWith(path, h->getPath());
			}
		});

	return (p != handlers.cend() ? p->get() : nullptr);
}

const Handler* VirtualHost::getErrorHandler(const ResponseCode error) const
{
	const auto search = errorHandlers.find(error);
	return (search != errorHandlers.end() ? search->second.get() : nullptr);
}
