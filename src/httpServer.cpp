#include "http.hpp"

#include<algorithm>
#include<fstream>
#include<thread>

#include<sys/stat.h>
#include<sys/types.h>

using namespace HTTP;

string HTTP::getLogTime()
{
	time_t now;
	tm ts;
	std::array<char, 80> buf;

	time(&now);
	ts = *localtime(&now);
	strftime(buf.data(), buf.size(), "%a %Y/%m/%d %H:%M:%S %Z", &ts);

	return string{buf.data()};
}

HTTPServer::HTTPServer(const unsigned short port)
	: socket(port)
{
	openLogs();

	socket.startListener();
	socket.setOnConnect([this](const Socket&& sock)
		{
			calls.emplace_back(*this, sock);
		});

	startCallsHandler();

	mkdir(ASSETS_FOLDER, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir(PAGES_FOLDER, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

void HTTPServer::openLogs()
{
	logs.open(LOGS_FILE, ofstream::app);
}

void HTTPServer::log(const string& s)
{
	const string time = getLogTime();

	cout << '[' << time << "] " << s << '\n';
	logs << '[' << time << "] " << s << '\n';
}

void HTTPServer::startCallsHandler()
{
	thread t([this]()
		{
			while(true) {
				iterateCalls();
				this_thread::sleep_for(1ms);
			}
		});

	t.detach();
}

void HTTPServer::iterateCalls()
{
	for(auto i = calls.begin(); i < calls.end();) {
		try {
			if(i->hasTimedOut()) {
				// TODO Response a REQUEST_TIMEOUT (passing by an error handler)

				i = calls.erase(i);
				continue;
			}

			if(i->isReady()) {
				i->handle();
				i = calls.erase(i);
			}
		} catch(const socket_exception& e) {}

		++i;
	}
}

VirtualHost& HTTPServer::createVirtualHost(const string& host)
{
	vhosts.emplace_back(host);
	return vhosts.back();
}

VirtualHost& HTTPServer::getVirtualHost(const string& host)
{
	const auto i = getVirtualHostIter(host);
	return (i != vhosts.end() ? *i : default_vhost);
}

const VirtualHost& HTTPServer::getVirtualHost(const Call& call) const
{
	const auto& parameters = call.getHTTPParameters();
	const auto i = parameters.find("Host");

	if(i == parameters.end()) throw bad_request_exception();

	const auto& host = i->second;

	const auto h = find_if(vhosts.cbegin(), vhosts.cend(), [&host](const VirtualHost& h)
		{
			return (h.getHost() == host);
		});

	return (h != vhosts.cend() ? *h : default_vhost);
}

vector<VirtualHost>::iterator HTTPServer::getVirtualHostIter(const string& host)
{
	auto h = find_if(vhosts.begin(), vhosts.end(), [&host](const VirtualHost& h)
		{
			return (h.getHost() == host);
		});

	return h;
}

void HTTPServer::removeVirtualHost(const string& host)
{
	const auto i = getVirtualHostIter(host);
	vhosts.erase(i);
}

void HTTPServer::registerSession(const Session& session)
{
	sessions.emplace(session.getUUID(), session);
}

Session* HTTPServer::getSession(const Call& call)
{
	for(auto it = sessions.begin(); it != sessions.end();) {
		auto& session = it->second;

		if(session.hasExpired()) {
			it = sessions.erase(it);
		} else {
			++it;
		}
	}

	const auto& cookies = call.getCookies();
	const auto search = cookies.find("session");

	if(search == cookies.end()) return nullptr;

	string uuid = search->second;
	uuid.resize(36);

	const auto s = sessions.find(uuid);

	if(s == sessions.end()) return nullptr;

	auto& session = s->second;
	session.refresh();

	return &session;
}

void HTTPServer::destroySession(const Session& session)
{
	sessions.erase(session.getUUID());
}

string HTTPServer::readPage(const string& path) const
{
	return readFile(PAGES_FOLDER + path);
}
