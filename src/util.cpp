#include "http.hpp"

#include<chrono>
#include<fstream>
#include<iostream>
#include<sstream>

unsigned long HTTP::getTimestamp()
{
	using namespace std::chrono;

	return time_point_cast<milliseconds>(steady_clock::now())
		.time_since_epoch().count();
}

bool HTTP::str_startsWith(const string& s1, const string& s2)
{
	if(s1.size() < s2.size()) return false;

	return equal(begin(s1), begin(s1) + static_cast<long>(s2.size()),
		begin(s2), end(s2));
}

void HTTP::str_replace(string& s, const string& from, const string& to)
{
	const size_t pos = s.find(from);
	if(pos != string::npos) s.replace(pos, from.length(), to);
}

string HTTP::makePathSafe(string path)
{
	if(!path.empty()) {
		if(path.front() == '/') path.erase(0, 1);
	}

	size_t pos;

	while((pos = path.find("..")) != string::npos) {
		path.erase(pos, 2);
	}
	
	return path;
}

string HTTP::getExtention(const string& path)
{
	const auto s = path.find_last_of('.');
	return (s != string::npos ? string(path.begin() + s, path.end()) : "");
}

string HTTP::readFile(const string& path)
{
	ifstream stream(path);

	stream.seekg(0, ios::end);
	const streamsize size = stream.tellg();
	stream.seekg(0);

	string buffer(static_cast<size_t>(size), '\0');
	stream.read(&buffer[0], size);

	return buffer;
}
