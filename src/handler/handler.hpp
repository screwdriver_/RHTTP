#ifndef HTTP_HANDLER_HPP
#define HTTP_HANDLER_HPP

#include "../http.hpp"

namespace HTTP
{
	using namespace std;

	class AssetsHandler : public Handler
	{
		public:
			inline AssetsHandler()
				: Handler("/assets", false)
			{}

			Response handle(Call& call, const VirtualHost& vhost) const override;
	};

	class StaticHandler : public Handler
	{
		public:
			inline StaticHandler(const string& path, const vector<string>& pages)
				: Handler(path, true), pages{pages}
			{}

			Response handle(Call& call, const VirtualHost&) const override;

		private:
			const vector<string> pages;
	};

	class StaticErrorHandler : public Handler
	{
		public:
			inline StaticErrorHandler(const ResponseCode error,
				const vector<string>& pages)
				: Handler("", false), error{error}, pages{pages}
			{}

			Response handle(Call& call, const VirtualHost&) const override;

		private:
			const ResponseCode error;
			const vector<string> pages;
	};

	class BadRequestHandler : public Handler
	{
		public:
			inline BadRequestHandler()
				: Handler("", false)
			{}

			Response handle(Call&, const VirtualHost&) const override;
	};

	class NotFoundHandler : public Handler
	{
		public:
			inline NotFoundHandler()
				: Handler("", false)
			{}

			Response handle(Call&, const VirtualHost&) const override;
	};

	class RequestTimeoutHandler : public Handler
	{
		public:
			inline RequestTimeoutHandler()
				: Handler("", false)
			{}

			Response handle(Call&, const VirtualHost&) const override;
	};
}

#endif
