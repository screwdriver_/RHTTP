#include "handler.hpp"

using namespace HTTP;

Response RequestTimeoutHandler::handle(Call&, const VirtualHost&) const
{
	Response response;
	response.setResponseCode(ResponseCode::REQUEST_TIMEOUT);
	response.setBody("<html><head><title>408 - Request Timeout</title></head><body><h1>Request Timeout</h1><p>The client took a too long time to send the request :/</p></body></html>");

	return response;
}
