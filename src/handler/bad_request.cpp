#include "handler.hpp"

using namespace HTTP;

Response BadRequestHandler::handle(Call&, const VirtualHost&) const
{
	Response response;
	response.setResponseCode(ResponseCode::BAD_REQUEST);
	response.setBody("<html><head><title>400 - Bad Request</title></head><body><h1>Bad Request</h1><p>The request the server received was ill-formed D:</p></body></html>");

	return response;
}
