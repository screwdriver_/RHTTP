#include "handler.hpp"

using namespace HTTP;

Response AssetsHandler::handle(Call& call, const VirtualHost& vhost) const
{
	Response response;

	string path = makePathSafe(call.getPath());
	ifstream stream(path);

	if(stream.is_open()) {
		response.setResponseCode(ResponseCode::OK);
		response.setContentType(getMimeType(getExtention(path)));
		response.setBody(readFile(path));

		return response;
	} else {
		return vhost.getErrorHandler(ResponseCode::NOT_FOUND)->handle(call, vhost);
	}
}
