#include "handler.hpp"

using namespace HTTP;

Response StaticErrorHandler::handle(Call& call, const VirtualHost&) const
{
	const auto& server = call.getServer();

	Response response;
	response.setResponseCode(error);

	for(const auto& p : pages) {
		response << server.readPage(p);
	}

	return response;
}
