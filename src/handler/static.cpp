#include "handler.hpp"

using namespace HTTP;

Response StaticHandler::handle(Call& call, const VirtualHost&) const
{
	const auto& server = call.getServer();
	Response response;

	for(const auto& p : pages) {
		response << server.readPage(p);
	}

	return response;
}
