#ifndef HTTP_HPP
#define HTTP_HPP

#include<fstream>
#include<iostream>
#include<memory>
#include<string>
#include<sstream>
#include<unordered_map>
#include<vector>

#include<socket.hpp>

#include<json.hpp>

#define LOGS_FILE "logs.log"

#define TIMEOUT 30000
#define DEFAULT_CONTENT_TYPE "text/html"

#define ASSETS_FOLDER "assets/"
#define PAGES_FOLDER "pages/"

#define DEFAULT_SESSION_LIFETIME 1800000

namespace HTTP
{
	using namespace std;
	using json = nlohmann::json;

	class HTTPServer;

	unsigned long getTimestamp();

	string getLogTime();

	string getDate();

	bool str_startsWith(const string& s1, const string& s2);
	void str_replace(string& s, const string& from, const string& to);

	string makePathSafe(string path);
	string getExtention(const string& path);

	string readFile(const string& path);

	string genUUID();

	class Session
	{
		public:
			inline const string& getUUID() const
			{
				return uuid;
			}

			inline unsigned long getStart() const
			{
				return start;
			}

			inline unsigned long getLastRefresh() const
			{
				return lastRefresh;
			}

			inline void refresh()
			{
				lastRefresh = getTimestamp();
			}

			inline unsigned long getLifetime() const
			{
				return lifetime;
			}

			inline void setLifetime(unsigned long lifetime)
			{
				this->lifetime = lifetime;
			}

			inline bool hasExpired() const
			{
				return (getTimestamp() >= lastRefresh + lifetime);
			}

			const string* getData(const string&& name) const;

			inline void setData(const string&& name, const string& value)
			{
				data[name] = value;
			}

		private:
			const string uuid = genUUID();

			const unsigned long start = getTimestamp();
			unsigned long lastRefresh = getTimestamp();
			unsigned long lifetime = DEFAULT_SESSION_LIFETIME;

			unordered_map<string, string> data;
	};

	enum ResponseCode : uint16_t
	{
		CONTINUE = 100,
		SWITCHING_PROTOCOLS = 101,
		PROCESSING = 102,

		OK = 200,
		CREATED = 201,
		ACCEPTED = 202,
		NON_AUTHORITATIVE_INFORMATION = 203,
		NO_CONTENT = 204,
		RESET_CONTENT = 205,
		PARTIAL_CONTENT = 206,
		MULTI_STATUS = 207,
		ALREADY_REPORTED = 208,
		IM_USED = 226,

		MULTIPLE_CHOICES = 300,
		MOVED_PERMANENTELY = 301,
		FOUND = 302,
		SEE_OTHER = 303,
		NOT_MODIFIED = 304,
		USE_PROXY = 305,
		SWITCH_PROXY = 306,
		TEMPORARY_REDIRECT = 307,
		PERMANENT_REDIRECT = 308,

		BAD_REQUEST = 400,
		UNAUTHORIZED = 401,
		PAYMENT_REQUIRED = 402,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		METHOD_NOT_ALLOWED = 405,
		NOT_ACCEPTABLE = 406,
		PROXY_AUTHENTICATION_REQUIRED = 407,
		REQUEST_TIMEOUT = 408,
		CONFLICT = 409,
		GONE = 410,
		LENGTH_REQUIRED = 411,
		PRECONDITION_FAILED = 412,
		PAYLOAD_TOO_LARGE = 413,
		URI_TOO_LONG = 414,
		UNSUPPORTED_MEDIA_TYPE = 415,
		RANGE_NOT_SATISFIABLE = 416,
		EXPECTATION_FAILED = 417,
		IM_A_TEAPOT = 418,
		MISDIRECTED_REQUEST = 421,
		UNPROCESSABLE_ENTITY = 422,
		LOCKED = 423,
		FAILED_DEPENDENCY = 424,
		UPGRADE_REQUIRED = 426,
		PRECONDITION_REQUIRED = 428,
		TOO_MANY_REQUESTS = 429,
		REQUEST_HEADER_FIELDS_TOO_LARGE = 431,
		UNAVAILABLE_FOR_LEGAL_REASONS = 451,

		INTERNAL_SERVER_ERROR = 500,
		NOT_IMPLEMENTED = 501,
		BAD_GATEWAY = 502,
		SERVICE_UNAVAILABLE = 503,
		GATEWAY_TIMEOUT = 504,
		HTTP_VERSION_NOT_SUPPORTED = 505,
		VARIANT_ALSO_NEGOTIATES = 506,
		INSUFFICIENT_STORAGE = 507,
		LOOP_DETECTED = 508,
		NOT_EXTENDED = 510,
		NETWORK_AUTHENTICATION_REQUIRED = 511
	};

	const unordered_map<uint16_t, string> responseCodes = {
		{CONTINUE, "Continue"},
		{SWITCHING_PROTOCOLS, "Switching Protocols"},
		{PROCESSING, "Processing"},
		{OK, "OK"},
		{CREATED, "Created"},
		{ACCEPTED, "Accepted"},
		{NON_AUTHORITATIVE_INFORMATION, "Non-Authoritative Information"},
		{NO_CONTENT, "No Content"},
		{RESET_CONTENT, "Reset Content"},
		{PARTIAL_CONTENT, "Partial Content"},
		{MULTI_STATUS, "Multi-Status"},
		{ALREADY_REPORTED, "Already Reported"},
		{IM_USED, "IM Used"},
		{MULTIPLE_CHOICES, "Multiple Choices"},
		{MOVED_PERMANENTELY, "Moved Permanently"},
		{FOUND, "Found"},
		{SEE_OTHER, "See Other"},
		{NOT_MODIFIED, "Not Modified"},
		{USE_PROXY, "Use Proxy"},
		{SWITCH_PROXY, "Switch Proxy"},
		{TEMPORARY_REDIRECT, "Temporary Redirect"},
		{PERMANENT_REDIRECT, "Permanent Redirect"},
		{BAD_REQUEST, "Bad Request"},
		{UNAUTHORIZED, "Unauthorized"},
		{PAYMENT_REQUIRED, "Payment Required"},
		{FORBIDDEN, "Forbidden"},
		{NOT_FOUND, "Not Found"},
		{METHOD_NOT_ALLOWED, "Method Not Allowed"},
		{NOT_ACCEPTABLE, "Not Acceptable"},
		{PROXY_AUTHENTICATION_REQUIRED, "Proxy Authentication Required"},
		{REQUEST_TIMEOUT, "Request Timeout"},
		{CONFLICT, "Conflict"},
		{GONE, "Gone"},
		{LENGTH_REQUIRED, "Length Required"},
		{PRECONDITION_FAILED, "Precondition Failed"},
		{PAYLOAD_TOO_LARGE, "Payload Too Large"},
		{URI_TOO_LONG, "URI Too Long"},
		{UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"},
		{RANGE_NOT_SATISFIABLE, "Range Not Satisfiable"},
		{EXPECTATION_FAILED, "Expectation Failed"},
		{IM_A_TEAPOT, "I'm a teapot"},
		{MISDIRECTED_REQUEST, "Misdirected Request"},
		{UNPROCESSABLE_ENTITY, "Unprocessable Entity"},
		{LOCKED, "Locked"},
		{FAILED_DEPENDENCY, "Failed Dependency"},
		{UPGRADE_REQUIRED, "Upgrade Required"},
		{PRECONDITION_REQUIRED, "Precondition Required"},
		{TOO_MANY_REQUESTS, "Too Many Requests"},
		{REQUEST_HEADER_FIELDS_TOO_LARGE, "Request Header Fields Too Large"},
		{UNAVAILABLE_FOR_LEGAL_REASONS, "Unavailable For Legal Reasons"},
		{INTERNAL_SERVER_ERROR, "Internal Server Error"},
		{NOT_IMPLEMENTED, "Not Implemented"},
		{BAD_GATEWAY, "Bad Gateway"},
		{SERVICE_UNAVAILABLE, "Service Unavailable"},
		{GATEWAY_TIMEOUT, "Gateway Timeout"},
		{HTTP_VERSION_NOT_SUPPORTED, "HTTP Version Not Supported"},
		{VARIANT_ALSO_NEGOTIATES, "Variant Also Negotiates"},
		{INSUFFICIENT_STORAGE, "Insufficient Storage"},
		{LOOP_DETECTED, "Loop Detected"},
		{NOT_EXTENDED, "Not Extended"},
		{NETWORK_AUTHENTICATION_REQUIRED, "Network Authentication Required"}
	};

	string responseCodeStr(const ResponseCode code);

	const unordered_map<string, string> extention_mimeTypes = {
		{"js", "application/javascript"},
		{"exe", "application/octet-stream"},
		{"app", "application/octet-stream"},
		{"ogg", "application/ogg"},
		{"pdf", "application/pdf"},
		{"xhtml", "application/xhtml+xml"},
		{"fla", "application/x-shockwave-flash"},
		{"flv", "application/x-shockwave-flash"},
		{"json", "application/json"},
		{"xml", "application/xml"},
		{"zip", "application/zip"},

		{"mpeg", "audio/mpeg"},
		{"mp3", "audio/mpeg"},

		{"mp4", "video/mpeg"},

		{"html", "text/html"},
		{"htm", "text/html"},
		{"css", "text/css"},
		{"csv", "text/csv"},
		{"txt", "text/plain"},

		{"png", "image/png"},
		{"jpeg", "image/jpeg"},
		{"jpg", "image/jpeg"},
		{"gif", "image/gif"}
	};

	string getMimeType(const string& extention);

	inline string getMimeType(const string&& extention)
	{
		return getMimeType(extention);
	}

	class Response
	{
		public:
			inline Response()
			{
				setContentType(DEFAULT_CONTENT_TYPE);
			}

			inline ResponseCode getResponseCode() const
			{
				return responseCode;
			}

			inline void setResponseCode(const ResponseCode code)
			{
				this->responseCode = code;
			}

			inline const unordered_map<string, string>& getHTTPParameters() const
			{
				return httpParameters;
			}

			const string* getHTTPParameter(const string&& key) const;
			void setHTTPParameter(const string&& key, const string& value);

			inline const string getContentType() const
			{
				return *getHTTPParameter("Content-Type");
			}

			inline void setContentType(const string& type)
			{
				setHTTPParameter("Content-Type", type);
			}

			inline void setContentType(const string&& type)
			{
				setContentType(type);
			}

			inline void redirect(const string& location)
			{
				responseCode = ResponseCode::TEMPORARY_REDIRECT;
				setHTTPParameter("Location", location);
			}

			inline void redirect(const string&& location)
			{
				redirect(location);
			}

			inline void setCookie(const string&& name, const string& value)
			{
				cookies.emplace(name, value);
			}

			inline void setSession(const Session& session)
			{
				setCookie("session", session.getUUID());
			}

			inline const string& getBody() const
			{
				return body;
			}

			inline void setBody(const string& body)
			{
				this->body = body;
			}

			inline void setBody(const string&& body)
			{
				setBody(body);
			}

			inline void operator<<(const string& content)
			{
				this->body += content;
			}

			inline void operator<<(const string&& content)
			{
				operator<<(content);
			}

			inline const unordered_map<string, string>& getReplacements() const
			{
				return replacements;
			}

			inline void setReplacement(const string& name, const string& value)
			{
				replacements[name] = value;
			}

			inline void setReplacement(const string&& name, const string& value)
			{
				setReplacement(name, value);
			}

			string toString(const bool content);

		private:
			ResponseCode responseCode = OK;

			unordered_map<string, string> httpParameters;
			unordered_map<string, string> cookies;

			string body;

			unordered_map<string, string> replacements;
	};

	class bad_request_exception : public exception
	{
		virtual const char* what() const noexcept
		{
			return "Bad request!";
		}
	};

	class Call
	{
		public:
			inline Call(HTTPServer& server, const Socket& socket)
				: server{&server}, socket{socket}
			{}

			inline ~Call()
			{
				socket.closeSocket();
			}

			inline unsigned long getTimestamp() const
			{
				return timestamp;
			}

			inline HTTPServer& getServer()
			{
				return *server;
			}

			inline Socket& getSocket()
			{
				return socket;
			}

			inline const string& getAddress() const
			{
				return socket.getAddress();
			}

			bool isReady();
			bool hasTimedOut();

			void handle();

			inline const string& getMethod() const
			{
				return method;
			}

			inline const string& getPath() const
			{
				return path;
			}

			inline const string& getHTTPVersion() const
			{
				return httpVersion;
			}

			inline const unordered_map<string, string>& getHTTPParameters() const
			{
				return httpParameters;
			}

			const string* getHTTPParameter(const string&& name) const;

			inline const unordered_map<string, string>& getGETArgs() const
			{
				return getArgs;
			}

			inline const unordered_map<string, string>& getPOSTArgs() const
			{
				return postArgs;
			}

			inline const unordered_map<string, string>& getCookies() const
			{
				return cookies;
			}

			inline const string& getBody() const
			{
				return body;
			}

			void operator<<(Response& response);

			inline void operator<<(Response&& response)
			{
				operator<<(response);
			}

		private:
			unsigned long timestamp = getTimestamp();

			HTTPServer* server = nullptr;
			Socket socket;

			string buffer;
			unsigned int i = 0;

			string method, path, httpVersion;

			unordered_map<string, string> httpParameters;

			unordered_map<string, string> getArgs;
			unordered_map<string, string> postArgs;

			unordered_map<string, string> cookies;

			string body;

			void readRequest();

			bool buffer_get(char& c);
			bool buffer_nextLine(string& s);

			void parseHeader();
			void parsePath();
			void parseBody();
			void parseCookies();
	};

	class VirtualHost;

	class Handler
	{
		public:
			inline Handler()
				: path{"/"}, absolute{false}
			{}

			inline Handler(const string& path, const bool absolute)
				: path{path}, absolute{absolute}
			{}

			inline Handler(const string&& path, const bool absolute)
				: Handler(path, absolute)
			{}

			inline const string& getPath() const
			{
				return path;
			}

			inline bool isAbsolute() const
			{
				return absolute;
			}

			virtual Response handle(Call& call, const VirtualHost& vhost) const = 0;

		private:
			const string path;
			const bool absolute;
	};

	class VirtualHost
	{
		public:
			inline VirtualHost()
				: VirtualHost("")
			{}

			VirtualHost(const string& host);

			inline VirtualHost(const string&& host)
				: VirtualHost(host)
			{}

			inline const string& getHost() const
			{
				return host;
			}

			template<class H>
			inline void registerHandler()
			{
				handlers.emplace_back(new H{});
			}

			template<class H>
			inline void registerErrorHandler(const ResponseCode error)
			{
				errorHandlers.emplace(error, new H{});
			}

			void registerStaticHandlers(const string& file);
			void registerHandler(const json& j);

			inline void registerStaticHandlers(const string&& file)
			{
				registerStaticHandlers(file);
			}

			const Handler* getHandlerForPath(const string& path) const;
			const Handler* getErrorHandler(const ResponseCode error) const;

		private:
			string host;

			vector<shared_ptr<Handler>> handlers;
			unordered_map<ResponseCode, shared_ptr<Handler>> errorHandlers;
	};

	class HTTPServer
	{
		public:
			HTTPServer(const unsigned short port);

			inline ~HTTPServer()
			{
				socket.closeSocket();
			}

			void log(const string& s);

			inline void log(const string&& s)
			{
				log(s);
			}

			inline unsigned short getPort() const
			{
				return socket.getPort();
			}

			inline VirtualHost& getDefaultVirtualHost()
			{
				return default_vhost;
			}

			VirtualHost& createVirtualHost(const string& host);

			inline VirtualHost& createVirtualHost(const string&& host)
			{
				return createVirtualHost(host);
			}

			inline const vector<VirtualHost>& getVirtualHosts() const
			{
				return vhosts;
			}

			VirtualHost& getVirtualHost(const string& host);

			inline VirtualHost& getVirtualHost(const string&& host)
			{
				return getVirtualHost(host);
			}

			const VirtualHost& getVirtualHost(const Call& call) const;

			vector<VirtualHost>::iterator getVirtualHostIter(const string& host);

			void removeVirtualHost(const string& host);

			inline void removeVirtualHost(const string&& host)
			{
				removeVirtualHost(host);
			}

			void registerSession(const Session& session);
			Session* getSession(const Call& call);
			void destroySession(const Session& session);

			string readPage(const string& path) const;

		private:
			ofstream logs;

			ServerSocket socket;

			VirtualHost default_vhost;
			vector<VirtualHost> vhosts;

			vector<Call> calls;

			unordered_map<string, Session> sessions;

			void openLogs();

			void startCallsHandler();
			void iterateCalls();
	};	
}

#endif
