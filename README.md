# RHTTP

RHTTP is a framework allowing to create websites using C++.



## Handlers

Handlers allow to define objects to handle defined requests.

There are two types of handlers:
- **Dynamic**: Those are defined into C++ code. They allow to change the content of the page that will be responded to the client according to the data it provides.
- **Static**: Those are defined into JSON files. They allow to quickly create static pages.

Each handler is either associated to a **path** or an **error code**.

If a handler is associated to a **path**, it will be used when:
- If it's **set as absolute**: The client asks for this exact path
- If it's **not set as absolute**: The client asks for a path which begins with the handler's path

If a handler is associated to an **error code**, it will be used when said error happens (and only if the call isn't already managed by a handler).



### Dynamic handlers

Dynamic handlers are defined like so:

```cpp
using namespace HTTP;

class IndexHandler : public Handler
{
	public:
		inline IndexHandler()
			: Handler("/", true)
		{}

		Response handle(Call&) const override
		{
			Response response;
			response.setBody("Hello world!");

			return response;
		}
};
```

After registering this handler, the webserver will return ``Hello world!`` when the path ``/`` is called.



### Static handlers

Static handlers allows to quickly create static pages. These handlers have to be registered into a JSON file.

Here is an example of static handlers declarations:

```json
{
	"handlers":[
		{
			"path":"/",
			"pages":[
				"home.html"
			]
		},
		{
			"error":404,
			"pages":[
				"404.html"
			]
		}
	]
}
```

In this example, the content of ``pages/home.html`` is shown when the page ``/`` is requested and the content of ``pages/404.html`` is shown in case of 404 error.



## Virtual hosts

Virtual hosts allow to specialize handlers for a specified hostname.
If the client tries to make a request to a hostname which has no associated virtual host, then the webserver uses the default virtual host.
